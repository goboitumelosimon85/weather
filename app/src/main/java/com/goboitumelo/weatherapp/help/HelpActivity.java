package com.goboitumelo.weatherapp.help;

import android.os.Bundle;

import com.goboitumelo.weatherapp.BaseActivity;
import com.goboitumelo.weatherapp.R;


public class HelpActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
	}

	@Override
	protected void updateUI() {
	}
}
