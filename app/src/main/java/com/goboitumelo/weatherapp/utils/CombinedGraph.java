package com.goboitumelo.weatherapp.utils;

public enum CombinedGraph {
    TEMPERATURE, WIND, RAINSNOW, PRESSURE;
}
